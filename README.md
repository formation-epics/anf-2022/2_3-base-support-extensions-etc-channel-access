# 2_3 base support extensions etc + channel access

> 60 mn

Cette partie commencera par une présentation de 30 mn sur le protocole de communication Channel
Access, et se poursuivra par une démonstration des outils associés à Channel Access ainsi que du
plugin Channel Access pour Wireshark. S'il reste du temps, alors une rapide présentation de PV
Access sera faite à la toute fin.

## Table des matières

<!-- vim-markdown-toc GitLab -->

* [Prérequis](#prérequis)
* [Références](#références)
* [Channel Access](#channel-access)
    * [Présentation Channel Access](#présentation-channel-access)
    * [Démo des outils Channel Access](#démo-des-outils-channel-access)
    * [Démo avec wireshark et tshark](#démo-avec-wireshark-et-tshark)
        * [Exemple avec une distribution Linux basée sur RHEL 7 (par exemple CentOS 7)](#exemple-avec-une-distribution-linux-basée-sur-rhel-7-par-exemple-centos-7)
        * [Exemple avec une distribution Linux basée sur RHEL 8 et plus (par exemple Rocky Linux 8)](#exemple-avec-une-distribution-linux-basée-sur-rhel-8-et-plus-par-exemple-rocky-linux-8)
* [PV Access](#pv-access)

<!-- vim-markdown-toc -->

## Prérequis

- [Rocky Linux 8](https://rockylinux.org/)
- ["2_1 base support extensions etc"](https://gitlab.com/formation-epics/anf-2022/2_1-base-support-extensions-etc)

## Références

- <https://epics-controls.org>
- <https://docs.epics-controls.org>
- <https://docs.epics-controls.org/en/latest/specs/specs.html>
- <https://docs.epics-controls.org/en/latest/specs/ca_protocol.html>
- <https://epics.anl.gov/index.php>
- <https://epics.anl.gov/docs/ca.php>
- <https://epics.anl.gov/base/R3-15/9-docs/CAref.html>
- <https://epics.anl.gov/base/R3-15/9-docs/CAref.html#CommandUtils>
- <https://epics.anl.gov/base/R3-15/9-docs/CAref.html#CommandTools>
- <https://wiki.wireshark.org/>
- <https://wiki.wireshark.org/CaptureFilters>
- <https://controlssoftware.sns.ornl.gov/training/2019_USPAS/Presentations/06%20Channel%20Access.pdf>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/10%20Channel%20Access.pdf>
- <https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/10a%20CA%20vs%20Records%20vs%20CAS.pdf>
- <https://epics.anl.gov/EpicsDocumentation/ExtensionsManuals/CaSnooper/CaSnooperAndOtherCADiagnostics.pdf>

## Channel Access

### Présentation Channel Access

> 30 mn

Cf. [présentation Channel Access](./presentation_channel_access.pdf) (basée sur
<https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/10%20Channel%20Access.pdf>
et sur
<https://controlssoftware.sns.ornl.gov/training/2022_USPAS/Presentations/10a%20CA%20vs%20Records%20vs%20CAS.pdf>)

### Démo des outils Channel Access

> 15 mn

Voici les principaux outils utilisés pour interagir avec Channel Access via un terminal :
```console
$ caget -h                      # récupère et affiche les valeurs d'une ou plusieurs PVs
$ caget $USER:calcExample       # pas aussi pratique que `dbpr` depuis l'IOC Shell à mon avis, puisqu'on ne peut pas afficher tous les fields en même temp (par exemple avec `epics> dbpr $USER:calcExample 10`)

$ caput -h                      # envoie une valeur à une PV
$ caput $USER:calcExample 42

$ camonitor -h                  # s'abonne et affiche les valeurs d'une ou plusieurs PVs à chaque changement
$ camonitor $USER:calcExample   # pour suivre l'évolution d'une PV dans le temps (on remarque que les champs d'alarmes bougent aussi)

$ cainfo -h                     # récupère et affiche différentes informations de connection sur une ou plusieurs PVs
$ cainfo $USER:calcExample  

$ catime $USER:calcExample      # pour tester la vitesse d'accès à une PV par le réseau
```

Voir aussi `caEventRate`, `caConnTest`, `ca_test`, `casw`, `acctst` (cf.
<https://epics.anl.gov/base/R3-15/9-docs/CAref.html#CommandUtils>) pour plus d'outils.

Si vous souhaitez découvrir la liste complète des utilitaires EPICS à explorer :
```console
$ cd /opt/epics/base/bin/linux-x86_64
$ tree
.
├── acctst
├── antelope
├── ascheck
├── assembleSnippets.pl
├── bldEnvData.pl
├── caConnTest
├── caEventRate
├── caget
├── cainfo
├── camonitor
├── caput
├── caRepeater
├── caRepeater.service
├── casw
├── ca_test
├── catime
├── convertRelease.pl
├── cvsclean.pl
├── dbdExpand.pl
├── dbdToHtml.pl
├── dbdToMenuH.pl
├── dbdToRecordtypeH.pl
├── dbExpand.pl
├── depclean.pl
├── dos2unix.pl
├── e_flex
├── epicsMakeMemFs.pl
├── epicsProve.pl
├── expandVars.pl
├── fullPathName.pl
├── genVersionHeader.pl
├── installEpics.pl
├── iocLogServer
├── makeAPIheader.pl
├── makeBaseApp.pl
├── makeBaseExt.pl
├── makeBpt
├── makeIncludeDbd.pl
├── makeMakefile.pl
├── makeRPath.py
├── makeTestfile.pl
├── mkmf.pl
├── msi
├── munch.pl
├── p2p
├── podRemove.pl
├── podToHtml.pl
├── pvcall
├── pvget
├── pvinfo
├── pvlist
├── pvmonitor
├── pvput
├── registerRecordDeviceDriver.pl
├── replaceVAR.pl
├── S99caRepeater
├── S99logServer
├── softIoc
├── softIocPVA
├── tap-to-junit-xml.pl
├── testFailures.pl
└── useManifestTool.pl
```

Petite parenthèse sur l'utilitaire `softIoc` qui permet de lancer un programme IOC "à la volée",
juste à partir d'un ou plusieurs `.db` (il est aussi possible de spécifier un ou plusieurs `.dbd`
associés, ainsi que les éventuelles macros pour chaque `.db`, on peut même spécifier un `.cmd` si
on le souhaite) :
```console
$ softIoc -h
$ softIoc $HOME/tops/tip-top/fooname/Db/tip.db
```

Seconde parenthèse au sujet de deux outils de diagnostic qui peuvent être intéressants :

- `casw` qui permet d'afficher les beacon anomalies sur votre réseau,
- `casnooper` qui permet de lister les PVs qui transitent sur votre réseau (et d'effectuer quelques
  statistiques dessus). Attention, `casnooper` ne fait pas partie de la base, il s'agit d'une
  extension à installer, cf. <https://epics.anl.gov/extensions/caSnooper/index.php> et
  <https://epics.anl.gov/EpicsDocumentation/ExtensionsManuals/CaSnooper/CaSnooperAndOtherCADiagnostics.pdf>.

### Démo avec wireshark et tshark

> 10mn

Il existe un plugin `wireshark` développé par la communauté EPICS :
[cashark](https://github.com/mdavidsaver/cashark). Il permet de faciliter la dissection des trames
Channel Access (et PV Access au passage). Voici comment l'utiliser

#### Exemple avec une distribution Linux basée sur RHEL 7 (par exemple CentOS 7)

Installez cashark :
```console
$ cd /opt/epics/extensions
$ git clone https://github.com/mdavidsaver/cashark.git
```

Puis installez wireshark :
```console
$ sudo yum install wireshark-gnome wireshark wireshark-devel
$ sudo usermod -aG wireshark $USER
$ newgrp wireshark
```

Vous pouvez alors lancer wireshark ainsi :
```console
$ wireshark -X lua_script:/opt/epics/extensions/cashark/ca.lua
```

Dans l'espace "Capture", sélectionnez l'interface `any` (ou `Loopback: lo`) puis cliquez sur
"Capture Options" et entrez le Capture Filter suivant : `host 127.0.0.1 and tcp port 5064`. Enfin
cliquez sur "Start" pour démarrer la capture.

Alternativement vous pouvez lancer tshark ainsi (pour ceux qui préfèrent en mode CLI) :
```console
$ tshark -i any -f "host 127.0.0.1 and tcp port 5064" -X lua_script:/opt/epics/extensions/cashark/ca.lua -PO ca
```

Vous pouvez maintenant lancer le programme IOC `fooname` de la partie précédente (cf. ["2_3 base support extensions
  etc"](https://gitlab.com/formation-epics/anf-2022/2_1-base-support-extensions-etc)) :
```console
$ cd $HOME/tops/tip-top/iocBoot/iocfooname/
$ ./st.cmd
```

Dans un autre terminal, vous pouvez à présent par exemple juste un simple `caget` :
```console
$ caget $USER:calcExample
```

Suite à ce `caget`, vous verrez immédiatement votre capture s'agiter. En particulier vous verrez
des trames "CA" dans la colonne "Protocol", entre les trames TCP. Si vous cliquez sur ces trames
CA, alors vous verrez des onglets "Channel Access" dans la partie "Dissector" de wireshark. Ces
onglets Channel Access vous aideront à disséquer plus facilement les trames CA (la lecture est
assez intuitive).

#### Exemple avec une distribution Linux basée sur RHEL 8 et plus (par exemple Rocky Linux 8)

cashark est régulièrement testé avec wireshark version `1.2.11`, `1.8.2`, `1.10.8`, `2.2.6`,
`2.6.0`, et `3.4.10` (au moment d'écrire cette démo). Par contre, cashark est un plugin écrit en
[lua](https://www.lua.org/), il dépend donc de la version de lua installée sur votre machine : lua
5.3+ par défaut sur toutes les distributions basées sur RHEL 8 et plus. Or il se trouve que
wireshark ne support lua que jusqu'à la version 5.2 et n'a pas l'intention de supporter lua 5.3
(pour l'instant) afin de respecter la rétro-compatibilité des scripts lua. Il semble donc difficile
d'utiliser cashark avec wireshark sur Rocky 8 par exemple.

Il existe cependant différents moyens pour contourner le problème :
<https://osqa-ask.wireshark.org/questions/44219/running-wireshark-with-lua-devel-53/>

En particulier, voici la solution proposée avec cashark :
<https://github.com/mdavidsaver/cashark/issues/9>

En résumé, il s'agit de re-compiler wireshark depuis ses sources, après l'avoir patché pour
utiliser un mode de compatibilité lua 5.1 :
```console
$ sudo dnf remove wireshark
$ sudo dnf install compat-lua compat-lua-devel compat-lua-libs # install lua compatibility tools for lua 5.1
$ cd /tmp
$ wget http://vault.centos.org/8-stream/AppStream/Source/SPackages/wireshark-2.6.2-12.el8.src.rpm
$ rpm -vv -Uvh /tmp/wireshark-2.6.2-12.el8.src.rpm # non root / non sudo !!!
$ wget https://github.com/mdavidsaver/cashark/files/6857254/wireshark-rh8-compat-lua.patch.txt
$ mv wireshark-rh8-compat-lua.patch.txt wireshark-rh8-compat-lua.patch
$ cd $HOME/rmpbuild
$ patch -p1 < /tmp/wireshark-rh8-compat-lua.patch
$ sudo dnf builddep $HOME/rpmbuild/SPECS/wireshark.spec # install build dependencies
$ rpmbuild -bb $HOME/rpmbuild/SPECS/wireshark.spec
```

À présent vous pouvez utiliser wireshark tel que décrit dans la section précédente (au moment de le
lancer).


## PV Access

Rapide présentation de PV Access avec le temps restant (s'il en reste). Cf. [présentation PV
Access](./presentation_pv_access.pdf).

